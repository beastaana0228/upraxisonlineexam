package org.project.bstaana.profileapp.base;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;

import org.project.bstaana.profileapp.R;

import static org.project.bstaana.profileapp.utils.Constants.UNDEFINED;

public class BaseDialogFragment extends DialogFragment {

    private DialogInterface.OnKeyListener backPressListener = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

            if ((keyCode ==  android.view.KeyEvent.KEYCODE_BACK))
            {
                //This is the filter
                if (event.getAction()!= KeyEvent.ACTION_DOWN) {

                    dismiss();
                    getActivity().finish();
                    return true;
                }
                else
                {
                    return true;
                }
            }
            else
                return false; // pass on to be processed as normal
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        if(finishActivityBackPressed()){
            getDialog().setOnKeyListener(backPressListener);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Activity activity = getActivity();
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MyAlertDialog);

        if (getTitleResId() != UNDEFINED) {
            builder.setTitle(getMessage(activity, getTitleResId()));
        }
        if (getMessageResId() != UNDEFINED) {
            builder.setMessage(getMessage(activity, getMessageResId()));
        } else {
            final View customView = getCustomView(activity);
            if (customView != null) {
                builder.setView(customView);
            }
        }
        if (getPositiveButtonResId() != UNDEFINED && getPositiveButtonListener() != null) {
            builder.setPositiveButton(
                    getMessage(activity, getPositiveButtonResId()),
                    getPositiveButtonListener()
            );
        }
        if (getNegativeButtonResId() != UNDEFINED && getNegativeButtonListener() != null) {
            builder.setNegativeButton(
                    getMessage(activity, getNegativeButtonResId()),
                    getNegativeButtonListener()
            );
        }

        // Create the AlertDialog object and return it
        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(isCancelableOnTouchOutside());

        return dialog;
    }

    private CharSequence getMessage(Activity activity, int resId) {
        CharSequence title = activity.getString(resId);
        return title;
    }

    @StringRes
    protected int getTitleResId() {
        return UNDEFINED;
    }

    @StringRes
    protected int getMessageResId() {
        return UNDEFINED;
    }

    @StringRes
    protected int getPositiveButtonResId() {
        return UNDEFINED;
    }

    @StringRes
    protected int getNegativeButtonResId() {
        return UNDEFINED;
    }

    protected boolean isCancelableOnTouchOutside() {
        return true;
    }

    protected DialogInterface.OnClickListener getPositiveButtonListener() {
        return null;
    }

    protected DialogInterface.OnClickListener getNegativeButtonListener() {
        return null;
    }

    protected View getCustomView(Activity activity) {
        return null;
    }

    protected boolean finishActivityBackPressed() {
        return false;
    }
}
