package org.project.bstaana.profileapp.base;

import rx.Subscriber;

public abstract class BaseSubscriber<T> extends Subscriber<T> {

    public BaseSubscriber() { }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(T t) {

    }
}
