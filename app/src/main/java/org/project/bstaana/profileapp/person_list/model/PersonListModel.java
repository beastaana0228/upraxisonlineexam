package org.project.bstaana.profileapp.person_list.model;

import org.project.bstaana.profileapp.base.BaseSubscriber;
import org.project.bstaana.profileapp.models.local.PersonDetails;
import org.project.bstaana.profileapp.models.response.PersonResponse;
import org.project.bstaana.profileapp.network.services.PersonService;
import org.project.bstaana.profileapp.person_list.contract.PersonListContract;
import org.project.bstaana.profileapp.person_list.presenter.PersonListPresenter.PersonListCallback;
import org.project.bstaana.profileapp.realm.RealmManager;
import org.project.bstaana.profileapp.utils.rx.RxSchedulers;

import java.util.List;

import io.realm.RealmObject;
import io.realm.RealmResults;

public class PersonListModel implements PersonListContract.Model {
    RealmManager realmManager;
    RxSchedulers rxSchedulers;
    PersonListCallback callback;
    BaseSubscriber subscriber;
    PersonService personService;

    public PersonListModel(RxSchedulers rxSchedulers, RealmManager realmManager, PersonService personService) {
        this.realmManager = realmManager;
        this.rxSchedulers = rxSchedulers;
        this.personService = personService;
    }

    @Override
    public List<PersonDetails> getPersonRealmList() {
        RealmResults<? extends RealmObject> realmResults = realmManager.getPersonListRealm();
        return (List<PersonDetails>) realmResults;
    }

    @Override
    public void deletePersonRealm() {
        realmManager.deletePersonResponseData();
    }

    @Override
    public void onSubmitPersonRequest(PersonListCallback callback) {
        this.callback = callback;
        subscriber = new PersonListSubscriber();
        personService.getPersons()
                .subscribeOn(rxSchedulers.internet())
                .observeOn(rxSchedulers.androidThread())
                .subscribe(subscriber);
    }

    @Override
    public BaseSubscriber getSubscriber() {
        return subscriber;
    }

    @Override
    public void savePersonRealm(PersonDetails response, int age) {
        PersonDetails personDetails = new PersonDetails(
                response.getPersonId(),
                response.getFirstName(),
                response.getLastName(),
                response.getBirthday(),
                response.getEmail(),
                response.getMobileNumber(),
                response.getAddress(),
                response.getContactPerson(),
                response.getContactPersonMobileNumber(),
                age);

        realmManager.addObject(personDetails);
    }

    private class PersonListSubscriber extends BaseSubscriber<PersonResponse> {

        @Override
        public void onStart() {
            super.onStart();
            callback.onStart();
        }

        @Override
        public void onNext(PersonResponse response) {
            super.onNext(response);
            callback.onCompleted(response);
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            callback.onError(e);
        }
    }
}
