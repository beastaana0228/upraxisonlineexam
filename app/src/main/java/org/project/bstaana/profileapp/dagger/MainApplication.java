package org.project.bstaana.profileapp.dagger;

import android.app.Application;
import android.content.Context;

import org.project.bstaana.profileapp.application.builder.AppComponent;
import org.project.bstaana.profileapp.application.builder.AppModule;

import java.util.HashMap;

import io.realm.Realm;

public class MainApplication extends Application {
    private static AppComponent appComponent;
    private static Context context;

    public static HashMap mTrackers = new HashMap();

    @Override
    public void onCreate() {
        super.onCreate();
        initApplicationComponent();
        Realm.init(this);
    }

    private void initApplicationComponent() {
//        appComponent = DaggerApplicationComponent.builder()
//                .applicationModule(new AppModule(this))
//                .build();
//        context = this;
    }

    public static Context getContext() {
        return context;
    }

    public static AppComponent getApplicationComponent() {
        return appComponent;
    }


}
