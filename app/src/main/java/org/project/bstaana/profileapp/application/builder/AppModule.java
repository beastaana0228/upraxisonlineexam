package org.project.bstaana.profileapp.application.builder;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    Context provideAppContext(Application application) {
        return application;
    }


}