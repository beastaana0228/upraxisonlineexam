package org.project.bstaana.profileapp.person_list.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.project.bstaana.profileapp.R;
import org.project.bstaana.profileapp.adapter.PersonItemAdapter;
import org.project.bstaana.profileapp.base.BaseRecyclerViewAdapter;
import org.project.bstaana.profileapp.models.local.PersonDetails;
import org.project.bstaana.profileapp.models.response.PersonResponse;
import org.project.bstaana.profileapp.person_details.view.PersonDetailsActivity;
import org.project.bstaana.profileapp.person_list.contract.PersonListContract;
import org.project.bstaana.profileapp.person_list.presenter.PersonListPresenter;
import org.project.bstaana.profileapp.utils.ErrorDialogFragment;
import org.w3c.dom.Text;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

import static org.project.bstaana.profileapp.utils.Constants.PERSON_ID;
import static org.project.bstaana.profileapp.utils.NetworkUtils.isOnline;

public class PersonActivity extends AppCompatActivity implements PersonListContract.View, BaseRecyclerViewAdapter.OnItemClickListener {

    @Inject
    PersonListPresenter presenter;

    @BindView(R.id.rvPersonList)
    RecyclerView rvPersonList;
    @BindView(R.id.loadingView)
    View loadingView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.tvNoResultFound)
    TextView tvNoResultFound;

    PersonItemAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);
        ButterKnife.bind(this);

        if (isOnline(this)) {
            initRequest();
        } else {
            validateOfflineMode();
        }
    }

    private void initRequest() {
        presenter.initPersonList();
    }

    private void validateOfflineMode() {
        presenter.onValidateOfflineMode();
    }

    @Override
    public void showErrorDialog() {
        ErrorDialogFragment fragment = ErrorDialogFragment.newInstance();
        fragment.show(getFragmentManager(), "");
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void initPersonListRecyclerView(List<PersonDetails> personDetailsList) {
        rvPersonList.setLayoutManager(new LinearLayoutManager(this));
        rvPersonList.setItemAnimator(new DefaultItemAnimator());
        adapter = new PersonItemAdapter(this, this);
        rvPersonList.setAdapter(adapter);
        adapter.addAll(personDetailsList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(Object object) {
        PersonDetails item = (PersonDetails) object;
        showPersonDetails(item);
    }

    private void showPersonDetails(PersonDetails personDetails) {
        Intent intent = new Intent(this, PersonDetailsActivity.class);
        intent.putExtra(PERSON_ID, personDetails.getPersonId());
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter.getSubscriber() != null) {
            presenter.getSubscriber().unsubscribe();
        }
    }

    @Override
    public void onBackPressed() {
        if (presenter.getSubscriber() != null) {
            presenter.getSubscriber().unsubscribe();
        }

        finish();
    }

    @Override
    public void showNoResultFound() {
        tvNoResultFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNoResultFound() {
        tvNoResultFound.setVisibility(View.GONE);
    }
}
