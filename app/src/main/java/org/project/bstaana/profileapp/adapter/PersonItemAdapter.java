package org.project.bstaana.profileapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.project.bstaana.profileapp.R;
import org.project.bstaana.profileapp.adapter.PersonItemAdapter.ItemViewHolder;
import org.project.bstaana.profileapp.base.BaseRecyclerViewAdapter;
import org.project.bstaana.profileapp.base.BaseViewHolder;
import org.project.bstaana.profileapp.models.local.PersonDetails;

/**
 * Created by Beatrice Sta Ana on 10/23/18.
 */

public class PersonItemAdapter extends BaseRecyclerViewAdapter<PersonDetails, ItemViewHolder> {

    private Context context;
    private OnItemClickListener listener;

    public PersonItemAdapter(Context context, OnItemClickListener listener) {
        super(listener);
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_person_item, parent, false);
        return new PersonItemAdapter.ItemViewHolder(itemView, mListener);
    }

    public class ItemViewHolder extends BaseViewHolder<PersonDetails> {
        private TextView tvPersonName;

        public ItemViewHolder(View itemView, OnItemClickListener listener) {
            super(itemView, listener);
            tvPersonName = itemView.findViewById(R.id.tvPersonName);
        }

        @Override
        public void renderView(PersonDetails item) {
            tvPersonName.setText(item.getFirstName() + " " + item.getLastName());
        }
    }
}
