package org.project.bstaana.profileapp.base;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public abstract class BasePresenter<V, M> {

    protected final V view;
    protected final M model;

    private CompositeSubscription subscription = new CompositeSubscription() ;

    public BasePresenter(V view, M model) {
        this.view = view;
        this.model = model;
    }

    /**
     * Contains common setup actions needed for all presenters, if any.
     * Subclasses may override this.
     */
    public void start() { }

    /**
     * Contains common cleanup actions needed for all presenters, if any.
     * Subclasses may override this.
     */
    public void stop() {
        subscription.clear();
    }

    protected void addDisposable(Subscription disposable) {
        subscription.add(disposable);
    }
}