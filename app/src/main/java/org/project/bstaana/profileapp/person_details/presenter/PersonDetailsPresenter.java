package org.project.bstaana.profileapp.person_details.presenter;

import org.project.bstaana.profileapp.base.BasePresenter;
import org.project.bstaana.profileapp.models.local.PersonDetails;
import org.project.bstaana.profileapp.person_details.contract.PersonDetailsContract;
import org.project.bstaana.profileapp.person_details.contract.PersonDetailsContract.View;
import org.project.bstaana.profileapp.person_details.model.PersonDetailsModel;

import javax.inject.Inject;


public class PersonDetailsPresenter extends BasePresenter<View, PersonDetailsModel> implements PersonDetailsContract.Presenter {
    PersonDetailsContract.View view;
    PersonDetailsModel model;

    public PersonDetailsPresenter(View view, PersonDetailsModel model) {
        super(view, model);
        this.view = view;
        this.model = model;
    }

    @Override
    public PersonDetails getPersonDetail(int personId) {
        return model.getPersonDetailsRealm(personId);
    }
}
