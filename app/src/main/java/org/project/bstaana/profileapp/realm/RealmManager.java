package org.project.bstaana.profileapp.realm;

import org.project.bstaana.profileapp.models.local.PersonDetails;
import org.project.bstaana.profileapp.models.response.PersonResponse;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

import static org.project.bstaana.profileapp.utils.Constants.PERSON_ID;

public class RealmManager {

    public void addObject(RealmObject object) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.insert(object);
        realm.commitTransaction();
    }

    public PersonDetails getPersonResponseRealm(int personId) {
        Realm realm = null;
        try{
            realm = Realm.getDefaultInstance();
            return realm.where(PersonDetails.class).equalTo(PERSON_ID, personId).findFirst();
        }finally {
            if(realm != null)
                realm.close();
        }
    }

    public RealmResults<RealmObject> getPersonListRealm() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults realmListData = realm.where(PersonDetails.class)
                .findAll();
        return realmListData;
    }

    public void deletePersonResponseData() {
        Realm realm = null;
        try{
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(PersonResponse.class);
                    realm.delete(PersonDetails.class);
                }
            });
        }finally {
            if (realm != null)
                realm.close();
        }
    }
}
