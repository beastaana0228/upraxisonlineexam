package org.project.bstaana.profileapp.utils;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import org.project.bstaana.profileapp.R;
import org.project.bstaana.profileapp.base.BaseDialogFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Beatrice Sta Ana on 10/26/18.
 */

public class ErrorDialogFragment extends BaseDialogFragment {

    public static ErrorDialogFragment newInstance() {
        final ErrorDialogFragment fragment = new ErrorDialogFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected View getCustomView(Activity activity) {
        final View v = activity.getLayoutInflater().inflate(R.layout.fragment_error_dialog, null);
        ButterKnife.bind(this, v);
        return v;
    }

    @OnClick(R.id.btnOkError)
    void onOkayClick() {
        dismiss();
    }

}
