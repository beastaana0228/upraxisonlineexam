package org.project.bstaana.profileapp.person_list.contract;

import org.project.bstaana.profileapp.base.BaseSubscriber;
import org.project.bstaana.profileapp.models.local.PersonDetails;
import org.project.bstaana.profileapp.models.response.PersonResponse;
import org.project.bstaana.profileapp.person_list.presenter.PersonListPresenter.PersonListCallback;

import java.util.List;

public interface PersonListContract {
    interface Model {
        List<PersonDetails> getPersonRealmList();
        void onSubmitPersonRequest(PersonListCallback callback);
        BaseSubscriber getSubscriber();
        void savePersonRealm(PersonDetails response, int age);
        void deletePersonRealm();
    }

    interface View {
        void showLoading();
        void hideLoading();
        void initPersonListRecyclerView(List<PersonDetails> personDetailsList);
        void showNoResultFound();
        void hideNoResultFound();
        void showErrorDialog();
    }

    interface Presenter {
        void initPersonList();
        BaseSubscriber getSubscriber();
        List<PersonDetails> getPersonListRealm();
        void onValidateOfflineMode();
    }
}
