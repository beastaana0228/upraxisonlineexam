package org.project.bstaana.profileapp.base;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerViewAdapter<M,V extends BaseViewHolder<M>> extends RecyclerView.Adapter<V> {

    private List<M> collection;
    protected OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(Object object);
    }

    public BaseRecyclerViewAdapter() {
        this.collection = new ArrayList<>();
    }

    public BaseRecyclerViewAdapter(OnItemClickListener listener) {
        this.collection = new ArrayList<>();
        this.mListener = listener;
    }

    /**
     * Add a collection of items to the list
     * @param collection
     */
    public void addAll(List<M> collection){
        this.collection.addAll(collection);
        notifyDataSetChanged();
    }

    /**
     * Add item to the end of the list
     * @param item
     */
    public void add(M item){
        this.collection.add(item);
        notifyItemInserted(this.collection.size() - 1);
    }

    /**
     * Insert an item to a specific position
     * @param item
     * @param position
     */
    public void add(M item, int position){
        this.collection.add(position, item);
        notifyItemInserted(position);
    }

    /**
     * Removed the item in the specified position
     * @param position
     */
    public void removeItem(int position){
        this.collection.remove(position);
        notifyItemRemoved(position);
    }


    /**
     * Update the item in the specified position
     * @param position
     * @param newObject
     */
    public void update(int position, M newObject){
        this.collection.set(position, newObject);
        notifyItemChanged(position);
    }


    @Override
    public void onBindViewHolder(V holder, int position) {
        M object = this.collection.get(position);

        holder.renderView(object);
        holder.setObject(object);

    }

    /**
     * Clear the items in the collection
     */
    public void clear(){
        this.collection.clear();
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return this.collection.size();
    }

}