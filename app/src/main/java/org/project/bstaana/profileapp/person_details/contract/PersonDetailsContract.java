package org.project.bstaana.profileapp.person_details.contract;

import org.project.bstaana.profileapp.models.local.PersonDetails;

public interface PersonDetailsContract {
    interface Model {
        PersonDetails getPersonDetailsRealm(int personId);
    }

    interface View {
        void populateData(PersonDetails personDetails);
    }

    interface Presenter {
        PersonDetails getPersonDetail(int personId);
    }
}
