package org.project.bstaana.profileapp.application.builder;

import org.project.bstaana.profileapp.dagger.module.PersonInfoModule;
import org.project.bstaana.profileapp.dagger.module.PersonModule;
import org.project.bstaana.profileapp.dagger.module.ViewModule;
import org.project.bstaana.profileapp.person_details.view.PersonDetailsActivity;
import org.project.bstaana.profileapp.person_list.view.PersonActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModule {

    // Activity and Fragments only
    @ContributesAndroidInjector(modules = {PersonModule.class, ViewModule.class})
    abstract PersonActivity bindPersonActivity();

    @ContributesAndroidInjector(modules = {PersonInfoModule.class, ViewModule.class})
    abstract PersonDetailsActivity bindPersonDetailsActivity();

}