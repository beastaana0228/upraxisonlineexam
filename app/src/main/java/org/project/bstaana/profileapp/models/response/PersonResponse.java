package org.project.bstaana.profileapp.models.response;

import com.google.gson.annotations.SerializedName;

import org.project.bstaana.profileapp.models.local.PersonDetails;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class PersonResponse extends RealmObject {

    @SerializedName("content")
    private RealmList<PersonDetails> personDetails;

    public PersonResponse() {}

    public PersonResponse(RealmList<PersonDetails> personDetails) {
        this.personDetails = personDetails;
    }

    public RealmList<PersonDetails> getPersonDetails() {
        return personDetails;
    }

    public void setPersonDetails(RealmList<PersonDetails> personDetails) {
        this.personDetails = personDetails;
    }
}
