package org.project.bstaana.profileapp.dagger.module;

import org.project.bstaana.profileapp.person_details.contract.PersonDetailsContract;
import org.project.bstaana.profileapp.person_details.model.PersonDetailsModel;
import org.project.bstaana.profileapp.person_details.presenter.PersonDetailsPresenter;
import org.project.bstaana.profileapp.realm.RealmManager;

import dagger.Module;
import dagger.Provides;

@Module
public class PersonInfoModule {

    @Provides
    PersonDetailsModel providePersonDetailsModel(RealmManager realmManager) {
        return new PersonDetailsModel(realmManager);
    }

    @Provides
    PersonDetailsPresenter providePresenterDetailsPresenter(PersonDetailsContract.View view, PersonDetailsModel model) {
        return new PersonDetailsPresenter(view, model);
    }

}
