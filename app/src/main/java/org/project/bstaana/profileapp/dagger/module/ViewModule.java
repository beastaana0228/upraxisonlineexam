package org.project.bstaana.profileapp.dagger.module;

import org.project.bstaana.profileapp.person_details.contract.PersonDetailsContract;
import org.project.bstaana.profileapp.person_details.view.PersonDetailsActivity;
import org.project.bstaana.profileapp.person_list.contract.PersonListContract;
import org.project.bstaana.profileapp.person_list.view.PersonActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModule {

    @Binds
    abstract PersonListContract.View providePersonListView(PersonActivity personActivity);


    @Binds
    abstract PersonDetailsContract.View providePersonDetailsView(PersonDetailsActivity personDetailsActivity);
}
