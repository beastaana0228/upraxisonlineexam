package org.project.bstaana.profileapp.person_details.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.project.bstaana.profileapp.R;
import org.project.bstaana.profileapp.models.local.PersonDetails;
import org.project.bstaana.profileapp.person_details.contract.PersonDetailsContract;
import org.project.bstaana.profileapp.person_details.presenter.PersonDetailsPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

import static org.project.bstaana.profileapp.utils.Constants.PERSON_ID;


public class PersonDetailsActivity extends AppCompatActivity implements PersonDetailsContract.View {
    private int personId;

    @Inject
    PersonDetailsPresenter presenter;

    @BindView(R.id.tvFirstName)
    TextView tvFirstName;
    @BindView(R.id.tvLastName)
    TextView tvLastName;
    @BindView(R.id.tvBirthday)
    TextView tvBirthday;
    @BindView(R.id.tvAge)
    TextView tvAge;
    @BindView(R.id.tvEmailAddress)
    TextView tvEmailAddress;
    @BindView(R.id.tvMobileNumber)
    TextView tvMobileNumber;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvContactPerson)
    TextView tvContactPerson;
    @BindView(R.id.tvContactPersonNumber)
    TextView tvContactPersonNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_details);
        ButterKnife.bind(this);

        getIntentData();
        initPersonDetails();

    }

    private void getIntentData() {
        if (getIntent() != null) {
            personId = getIntent().getIntExtra(PERSON_ID, 0);
        }
    }

    private void initPersonDetails() {
        if (personId != 0) {
            PersonDetails info = presenter.getPersonDetail(personId);
            populateData(info);
        }
    }

    @Override
    public void populateData(PersonDetails personDetails) {
        tvFirstName.setText(personDetails.getFirstName());
        tvLastName.setText(personDetails.getLastName());
        tvBirthday.setText(personDetails.getBirthday());
        tvAge.setText(personDetails.getAge() + " years old");
        tvEmailAddress.setText(personDetails.getEmail());
        tvMobileNumber.setText(personDetails.getMobileNumber());
        tvAddress.setText(personDetails.getAddress());
        tvContactPerson.setText(personDetails.getContactPerson());
        tvContactPersonNumber.setText(personDetails.getContactPersonMobileNumber());
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
