package org.project.bstaana.profileapp.application.builder;

import android.app.Application;

import org.project.bstaana.profileapp.application.AppController;
import org.project.bstaana.profileapp.dagger.RestModule;
import org.project.bstaana.profileapp.network.services.PersonService;
import org.project.bstaana.profileapp.person_details.view.PersonDetailsActivity;
import org.project.bstaana.profileapp.person_list.view.PersonActivity;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules =
        {
                AndroidSupportInjectionModule.class,
                AppModule.class,
                BuildersModule.class,
                RxModule.class,
                RestModule.class,
                RealmManagerModule.class,
                NetworkModule.class
        })
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    void inject(AppController appController);

}

