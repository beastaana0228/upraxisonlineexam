package org.project.bstaana.profileapp.network.services;

import org.project.bstaana.profileapp.models.local.PersonDetails;
import org.project.bstaana.profileapp.models.response.PersonResponse;

import retrofit2.http.GET;
import rx.Observable;

public interface PersonService {

    @GET("/persons.json?key=340d6680")
    Observable<PersonResponse> getPersons();
}
