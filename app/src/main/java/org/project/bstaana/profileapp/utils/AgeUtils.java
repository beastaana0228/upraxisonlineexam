package org.project.bstaana.profileapp.utils;

import java.util.Calendar;

public class AgeUtils {

    public static int getCurrentYear() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(Calendar.getInstance().getTime());
        return calendar.get(Calendar.YEAR);
    }
}
