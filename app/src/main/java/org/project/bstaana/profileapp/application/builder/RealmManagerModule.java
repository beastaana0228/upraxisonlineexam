package org.project.bstaana.profileapp.application.builder;

import org.project.bstaana.profileapp.realm.RealmManager;

import dagger.Module;
import dagger.Provides;

@Module
public class RealmManagerModule {
    @Provides
    RealmManager provideRealmManager() {
        return new RealmManager();
    }
}