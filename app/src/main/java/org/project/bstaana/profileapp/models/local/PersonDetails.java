package org.project.bstaana.profileapp.models.local;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class PersonDetails extends RealmObject {

    @SerializedName("id")
    private int personId;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("birthday")
    private String birthday;
    @SerializedName("email")
    private String email;
    @SerializedName("mobile_number")
    private String mobileNumber;
    @SerializedName("address")
    private String address;
    @SerializedName("contact_person")
    private String contactPerson;
    @SerializedName("contact_person_mobile_number")
    private String contactPersonMobileNumber;

    private int age;

    public PersonDetails() { }

    public PersonDetails(int personId,
                         String firstName,
                         String lastName,
                         String birthday,
                         String email,
                         String mobileNumber,
                         String address,
                         String contactPerson,
                         String contactPersonMobileNumber,
                         int age) {

        this.personId = personId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.address = address;
        this.contactPerson = contactPerson;
        this.contactPersonMobileNumber = contactPersonMobileNumber;
        this.age = age;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactPersonMobileNumber() {
        return contactPersonMobileNumber;
    }

    public void setContactPersonMobileNumber(String contactPersonMobileNumber) {
        this.contactPersonMobileNumber = contactPersonMobileNumber;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
