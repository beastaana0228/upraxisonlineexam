package org.project.bstaana.profileapp.dagger.module;

import org.project.bstaana.profileapp.network.services.PersonService;
import org.project.bstaana.profileapp.person_list.contract.PersonListContract;
import org.project.bstaana.profileapp.person_list.model.PersonListModel;
import org.project.bstaana.profileapp.person_list.presenter.PersonListPresenter;
import org.project.bstaana.profileapp.realm.RealmManager;
import org.project.bstaana.profileapp.utils.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class PersonModule {

    @Provides
    PersonListModel providePersonModel(RxSchedulers rxSchedulers,
                                       RealmManager realmManager,
                                       PersonService personService) {
        return new PersonListModel(rxSchedulers, realmManager, personService);
    }

    @Provides
    PersonListPresenter providePersonPresenter(PersonListContract.View view, PersonListModel model) {
        return new PersonListPresenter(view, model);
    }

}
