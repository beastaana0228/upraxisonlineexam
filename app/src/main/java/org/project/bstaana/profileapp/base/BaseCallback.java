package org.project.bstaana.profileapp.base;

public abstract class BaseCallback<T, E> {

    public BaseCallback() {
    }
    public abstract void onStart();

    public abstract void onCompleted(T t);

    public  abstract void onError(E e);

}
