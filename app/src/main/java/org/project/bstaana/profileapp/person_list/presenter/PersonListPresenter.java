package org.project.bstaana.profileapp.person_list.presenter;

import android.util.Log;

import org.project.bstaana.profileapp.base.BaseCallback;
import org.project.bstaana.profileapp.base.BasePresenter;
import org.project.bstaana.profileapp.base.BaseSubscriber;
import org.project.bstaana.profileapp.models.local.PersonDetails;
import org.project.bstaana.profileapp.models.response.PersonResponse;
import org.project.bstaana.profileapp.person_list.contract.PersonListContract;
import org.project.bstaana.profileapp.person_list.contract.PersonListContract.View;
import org.project.bstaana.profileapp.person_list.model.PersonListModel;

import java.util.List;

import io.realm.RealmList;

import static org.project.bstaana.profileapp.utils.AgeUtils.getCurrentYear;

public class PersonListPresenter extends BasePresenter<View, PersonListModel> implements PersonListContract.Presenter {

    PersonListContract.View view;
    PersonListModel model;
    private String validAge;

    public PersonListPresenter(View view, PersonListModel model) {
        super(view, model);
        this.view = view;
        this.model = model;
    }

    @Override
    public void initPersonList() {
        if (getPersonListRealm().size() > 0) {
            view.initPersonListRecyclerView(getPersonListRealm());
        } else {
            model.onSubmitPersonRequest(new PersonListCallback());
        }
    }

    @Override
    public void onValidateOfflineMode() {
        if (getPersonListRealm().size() > 0) {
            view.initPersonListRecyclerView(getPersonListRealm());
        } else {
            view.showErrorDialog();
            view.showNoResultFound();
        }
    }

    @Override
    public BaseSubscriber getSubscriber() {
        return model.getSubscriber();
    }

    @Override
    public List<PersonDetails> getPersonListRealm() {
        return model.getPersonRealmList();
    }

    private void validatePersonInfoResponse(List<PersonDetails> personDetails) {
        RealmList<PersonDetails> personDetailsList = new RealmList<>();

        for (PersonDetails response : personDetails) {

            int age = getCurrentYear() - getBirthdayYear(response.getBirthday());

            personDetailsList.add(new PersonDetails(
                    response.getPersonId(),
                    response.getFirstName(),
                    response.getLastName(),
                    response.getBirthday(),
                    response.getEmail(),
                    response.getMobileNumber(),
                    response.getAddress(),
                    response.getContactPerson(),
                    response.getContactPersonMobileNumber(),
                    age));

            model.savePersonRealm(response, age);
        }
    }

    private int getBirthdayYear(String birhday) {
        int year = Integer.parseInt(birhday.substring(6, 10));
        return year;
    }



    public class PersonListCallback extends BaseCallback<PersonResponse, Throwable> {

        @Override
        public void onStart() {
            view.showLoading();
        }

        @Override
        public void onCompleted(PersonResponse response) {
            view.hideLoading();
            view.hideNoResultFound();

            validatePersonInfoResponse(response.getPersonDetails());
            view.initPersonListRecyclerView(getPersonListRealm());

            Log.d("TAGGG", "onCompleted: " + response.getPersonDetails().size());
        }

        @Override
        public void onError(Throwable throwable) {
            view.hideLoading();
            view.showNoResultFound();
        }
    }
}
