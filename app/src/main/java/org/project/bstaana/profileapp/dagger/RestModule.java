package org.project.bstaana.profileapp.dagger;

import org.project.bstaana.profileapp.network.services.PersonService;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RestModule {

    public static final String BASE_URL = "https://my.api.mockaroo.com";

    @Provides
    public Retrofit provideRestAdapter(
            final GsonConverterFactory gson,
            final RxJavaCallAdapterFactory rxAdapter,
            final Cache cache
    ) {
        OkHttpClient.Builder OkClientBuilder = new OkHttpClient().newBuilder();
        OkClientBuilder.readTimeout(60, TimeUnit.SECONDS);
        OkClientBuilder.connectTimeout(60, TimeUnit.SECONDS);
        OkClientBuilder.writeTimeout(60, TimeUnit.SECONDS);
        OkClientBuilder.cache(cache);

        final Retrofit.Builder builder = new Retrofit.Builder()
                .client(OkClientBuilder.build())
                .addCallAdapterFactory(rxAdapter)
                .addConverterFactory(gson)
                .baseUrl(BASE_URL);
        return builder.build();
    }

    @Provides
    public PersonService providePersonService(final Retrofit retrofit) {
        return retrofit.create(PersonService.class);
    }
}
