package org.project.bstaana.profileapp.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class BaseViewHolder<M> extends RecyclerView.ViewHolder implements View.OnClickListener{

    private BaseRecyclerViewAdapter.OnItemClickListener mListener;
    private M mObject;

    public BaseViewHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);
    }

    public BaseViewHolder(View itemView, BaseRecyclerViewAdapter.OnItemClickListener listener) {
        super(itemView);

        itemView.setOnClickListener(this);
        this.mListener = listener;
    }

    @Override
    public void onClick(View view) {

        if(mListener != null){
            mListener.onItemClick(mObject);
        }
    }

    public void setObject(M item){

        this.mObject = item;
    }

    public abstract void renderView(M item);
}