package org.project.bstaana.profileapp.person_details.model;

import org.project.bstaana.profileapp.models.local.PersonDetails;
import org.project.bstaana.profileapp.person_details.contract.PersonDetailsContract;
import org.project.bstaana.profileapp.realm.RealmManager;

import io.realm.RealmObject;
import io.realm.RealmResults;

public class PersonDetailsModel implements PersonDetailsContract.Model {
    RealmManager realmManager;

    public PersonDetailsModel(RealmManager realmManager) {
        this.realmManager = realmManager;
    }

    @Override
    public PersonDetails getPersonDetailsRealm(int personId) {
        return realmManager.getPersonResponseRealm(personId);
    }
}
