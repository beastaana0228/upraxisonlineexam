package org.project.bstaana.profileapp.application.builder;

import org.project.bstaana.profileapp.utils.rx.AppRxSchedulers;
import org.project.bstaana.profileapp.utils.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class RxModule {

    @Provides
    RxSchedulers provideRxSchedulers() {
        return new AppRxSchedulers();
    }
}
